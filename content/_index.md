## Site de documentation auto-généré

Ce site a été créé par JPG avec [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) en environ 3 minutes top chrono. Il utilise le thème
`beautifulhugo` par défaut mais on peut adapter facilement.

Pour l'instant, je ne mets aucun contenu, c'est juste pour faire voir qu'on peut changer tout sans difficulté.

Voici le **menu** à exécuter

## Titre

- about
- pages
